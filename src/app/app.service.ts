import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AppService {
allList:any=[]
  constructor(public toastr:ToastrService) {
  }
  get(key:any){
    let all=localStorage.getItem(key)
    if(all){
      this.allList=JSON.parse(all)
      return this.allList
    }
  }

  create(key:any,value:any){
    localStorage.setItem(key,JSON.stringify(value))
    return this.toastr.success(key+' created successfully')
  }

  delete(key:any,index:any){
  let all=this.get(key)
  if(all){
    all.splice(index,1)
    this.create(key,all)
    return this.toastr.success(key+' deleted successfully')
  }
  return
  }

}
