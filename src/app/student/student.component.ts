import { Component } from '@angular/core';
import { AppService } from '../app.service'
import { FormGroup, FormControl, Validators } from '@angular/forms'
declare var $: any

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent {
  allUsers: any = [];
  index: any;
  submitted: Boolean = false;
  studentForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    phone: new FormControl('', [Validators.required, Validators.minLength(10), Validators.pattern(/^[0-9]+$/)]),
    startTime: new FormControl('', Validators.required),
    endTime: new FormControl('', Validators.required),
    Id: new FormControl('', Validators.required),
    date: new FormControl('', Validators.required),
    distance: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]+$/)])

  })
  constructor(public appService: AppService) {
    this.allUsers = this.appService.get('Students')
  }


  create() {
    if (this.studentForm.valid) {
      let all = this.appService.get('Students')
      if (!all) {
        all = []
      }
      all.push(this.studentForm.value)
      this.appService.create('Students', all)
      $('#studentModal').hide()
      this.allUsers = this.appService.get('Students')
    }
    else {
      this.submitted = true
      // $('#studentModal').hide()
    }
  }

  indexes(i: any) {
    this.index = i
    console.log(this.index, '........index')
  }

  delete() {
    this.appService.delete('Students', this.index)
    $('#deleteStudentModal').hide()
    this.allUsers = this.appService.get('Students')
    return
  }
}
